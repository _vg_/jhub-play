if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
  . "$HOME/.bashrc"
    fi  
fi

alias  top='/usr/bin/top -d 1'
alias  df='/bin/df -h'
export  PS_OPTIONS=' -eo ruser,rgroup,pid,stat,%cpu,nice,%mem,psr,lwp,nlwp,vsz,rss,tt,start,time,args '
alias  ps='/bin/ps -eo ruser,rgroup,pid,stat,%cpu,nice,%mem,psr,lwp,nlwp,vsz,rss,tt,start,time,args '
alias  psl='ps    | lis'
export  LESS=R
export  LESSCHARSET='utf-8'
export  PAGER="/bin/less -Mi"
alias  l='/bin/less  -Mi'
alias  li='/bin/less  -Mi'
alias  lis='/bin/less  -MiS'
alias  lin='/bin/less  -MiSN'
alias  ll='/bin/ls  -la --color=auto'
alias  lld='/bin/ls  -lad --color=auto'
alias  llh='/bin/ls  -lah --color=auto'
alias  llg='/bin/ls  -la --color=auto | egrep '
alias  llgi='/bin/ls  -la --color=auto | egrep -i '
alias  llfg='/bin/ls  -la --color=auto | fgrep '
alias  llfgi='/bin/ls  -la --color=auto | fgrep -i '
alias  ltr='/bin/ls  -lactr --color=auto'
alias  ltrh='/bin/ls  -lactrh --color=auto'
alias  lsr='/bin/ls  -laSr --color=auto'
alias  lsrh='/bin/ls  -laSrh --color=auto'
alias  lur='/bin/lS  -laur --color=auto'
