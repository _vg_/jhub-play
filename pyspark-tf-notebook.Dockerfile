FROM	jupyter/pyspark-notebook

# create conda profile
COPY	--chown=jovyan:users .bash_profile /home/jovyan/
RUN	bash -l -c "	\
			conda init bash	&&\
			source ~/.bashrc	&&\
			conda  create -n myenv python=3.7	\
		"

# Install TF
RUN conda install -n base 'tensorflow=2.1.0' -y	&&\
    conda clean --all -f -y 						&&\
    fix-permissions $CONDA_DIR 					&&\
    fix-permissions /home/$NB_USER
