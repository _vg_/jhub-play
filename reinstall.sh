#!/usr/bin/env bash

# k8s JHub remove completely
kubectl delete ns jhtf
minikube -p tf-play delete

# k8s start
minikube -p tf-play start --kubernetes-version=v1.15.2 --cpus=3 --memory=8192m

# Helm2 install
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
kubectl --namespace kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller --wait
kubectl patch deployment tiller-deploy --namespace=kube-system --type=json --patch='[{"op": "add", "path": "/spec/template/spec/containers/0/command", "value": ["/tiller", "--listen=localhost:44134"]}]'
helm version

# JHub install
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
sleep 10
helm upgrade jhtf jupyterhub/jupyterhub --namespace jhtf --version=0.8.2 --values config.yaml --timeout=3600 --install

# JHub check deployment
sleep 30
kubectl -n jhtf get po
minikube -p tf-play service list
